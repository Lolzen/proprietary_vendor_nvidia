# Copyright (c) 2010-2016, NVIDIA Corporation.  All rights reserved.
#
# NVIDIA Corporation and its licensors retain all intellectual property
# and proprietary rights in and to this software, related documentation
# and any modifications thereto.  Any use, reproduction, disclosure or
# distribution of this software and related documentation without an express
# license agreement from NVIDIA Corporation is strictly prohibited.
#

# this is nvcamera module version
__version__ = "4.1.2"

# Version history:
# "2.0.1" to "3.0.1" :  Change 681350 (nvcs: Changes for Automotive support)
# "3.0.1" to "3.0.2" :  Change 736886 (nvcs: nvcs: Add CamProperty PROP_HDR_RATIO_OVERRIDE)
# "3.0.2" to "4.0.0" :  Change 496301 (nvcs: Add streaming and per frame apis to nvcs)
# "4.0.0" to "4.0.1" :  Change 785680 (nvcs: sensor mode support to capture script)
# "4.0.1" to "4.1.0" :  Change 807634 (nvcs: add support for aperture properties)
# "4.1.0" to "4.1.1" :  Change 1020701 (nvcs: WAR: choose default preview mode to be max resolution always)
# "4.1.1" to "4.1.2" :  Change 1127023 (nvcs: fix a logic to decide max resolution)

class NvCameraVersion:
    "Version Class"

    def getMajor(self):
        ver = __version__.split('.')
        assert len(ver) >= 3
        return int(ver[0])

    def getMinor(self):
        ver = __version__.split('.')
        assert len(ver) >= 3
        return int(ver[2])

